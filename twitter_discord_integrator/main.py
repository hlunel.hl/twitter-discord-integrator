from loguru import logger

from twitter_discord_integrator import discord_service
from twitter_discord_integrator import twitter_service


def main():
    if tweets := twitter_service.get_tweets():
        logger.info(f"Found {len(tweets)} tweets")
        discord_service.send_tweets(tweets)


if __name__ == "__main__":
    main()
