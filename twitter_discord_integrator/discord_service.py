import json
import time

from loguru import logger
from pytz import UTC, timezone
from discord_webhook import DiscordWebhook

from twitter_discord_integrator.config import (
    WEBHOOK_URL,
    DISCORD_PING_ROLE_ID,
    KEYWORDS,
)
from twitter_discord_integrator.twitter_service import Tweet

EUROPE_PARIS = timezone("Europe/Paris")
AMERICA_MONTREAL = timezone("America/Montreal")

DISCORD_WEBHOOK = DiscordWebhook(url=WEBHOOK_URL)


def send_tweets(tweets: list[Tweet]) -> None:
    for tweet in tweets:
        if keywords := _contained_keywords(tweet.text):
            logger.info(
                f"Sending tweet with id {tweet.id} because it contains {keywords}"
            )
            _send_tweet(_build_content(tweet=tweet, keywords=keywords))
        else:
            logger.info(
                f"Not sending tweet with id {tweet.id} because it doesn't contain any keyword"
            )


def _send_tweet(concatenated_contents: str) -> None:
    DISCORD_WEBHOOK.content = concatenated_contents

    while True:
        response = DISCORD_WEBHOOK.execute()
        if response.status_code == 200:
            break
        if response.status_code == 429:
            response_content = json.loads(response.content)
            time_to_wait = response_content["retry_after"] / 1000
            time.sleep(time_to_wait)


def _build_content(tweet: Tweet, keywords: list[str]) -> str:
    return (
        f"<@&{DISCORD_PING_ROLE_ID}>\n**KEYWORD(S) DETECTED**: {keywords}\n"
        + f"**{tweet.author}** tweeted this at\n"
        + f"\t({tweet.created_at.astimezone(UTC)} {UTC.zone})\n"
        + f"\t({tweet.created_at.astimezone(EUROPE_PARIS)} {EUROPE_PARIS.zone})\n"
        + f"\t({tweet.created_at.astimezone(AMERICA_MONTREAL)} {AMERICA_MONTREAL.zone})\n"
        + f"http://twitter.com/{tweet.author}/status/{tweet.id}\n\n"
    )


def _contained_keywords(text: str) -> list[str]:
    lowercase_text = text.lower()
    return [keyword for keyword in KEYWORDS if keyword in lowercase_text]
