from datetime import datetime, timedelta

from dateutil import parser as date_parser
from dateutil.tz import UTC
from twitter import Api as TwitterApi

from twitter_discord_integrator.config import (
    TWITTER_CONSUMER_SECRET,
    TWITTER_CONSUMER_KEY,
    TWITTER_ACCESS_TOKEN_KEY,
    TWITTER_ACCESS_TOKEN_SECRET,
    LAST_X_MINUTES_VALUE,
)
from twitter_discord_integrator.tweet import Tweet

TWITTER_API = TwitterApi(
    consumer_key=TWITTER_CONSUMER_KEY,
    consumer_secret=TWITTER_CONSUMER_SECRET,
    access_token_key=TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret=TWITTER_ACCESS_TOKEN_SECRET,
)


def get_tweets() -> list[Tweet]:
    last_x_minutes = _get_last_x_minutes()
    return [
        Tweet(
            author=status.user.screen_name,
            created_at=date_parser.parse(status.created_at),
            text=status.text,
            id=status.id,
        )
        for status in _get_statuses()
        if date_parser.parse(status.created_at) >= last_x_minutes
    ]


def _get_last_x_minutes():
    return (datetime.utcnow() - timedelta(minutes=LAST_X_MINUTES_VALUE)).replace(
        tzinfo=UTC
    )


def _get_statuses():
    statuses = TWITTER_API.GetHomeTimeline(exclude_replies=True)
    statuses.reverse()
    return statuses
