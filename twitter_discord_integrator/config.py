import os
from typing import List, Final

# Twitter
TWITTER_CONSUMER_KEY: Final[str] = os.getenv("TWITTER_CONSUMER_KEY")
TWITTER_CONSUMER_SECRET: Final[str] = os.getenv("TWITTER_CONSUMER_SECRET")
TWITTER_ACCESS_TOKEN_KEY: Final[str] = os.getenv("TWITTER_ACCESS_TOKEN_KEY")
TWITTER_ACCESS_TOKEN_SECRET: Final[str] = os.getenv("TWITTER_ACCESS_TOKEN_SECRET")

# Discord
WEBHOOK_BASE_URL: Final[str] = "https://discord.com/api/webhooks/"
DISCORD_WEBHOOK_ID: Final[str] = os.getenv("DISCORD_WEBHOOK_ID")
WEBHOOK_URL: Final[str] = WEBHOOK_BASE_URL + DISCORD_WEBHOOK_ID
DISCORD_PING_ROLE_ID: Final[str] = os.getenv("DISCORD_PING_ROLE_ID")

# App
LAST_X_MINUTES_VALUE: Final[int] = int(os.getenv("LAST_X_MINUTES_VALUE"))
KEYWORDS: List[str] = os.getenv("KEYWORDS").split(" ")
