# Twitter discord integrator

## Summary

This project is used to monitor the feed page of a Twitter account in order to send Discord alerts if any tweet in the
feed contains one or more configured keywords.

The specific use case for which I created this project is to monitor Crypto related tweets.

## General requirements

- Setup a Twitter account who follows users whose tweets you want to monitor
- Setup a Discord server on which you want to receive the alert along with a specific Discord role to be pinged

## Local usage

### Requirements

- Python 3.9+ installed
- Poetry installed

### Setup

Execute the following command:

```shell
> poetry install
```

Add and complete the following .env file:

```dotenv
TWITTER_CONSUMER_KEY=<TWITTER_CONSUMER_KEY>
TWITTER_CONSUMER_SECRET=<TWITTER_CONSUMER_SECRET>
TWITTER_ACCESS_TOKEN_KEY=<TWITTER_ACCESS_TOKEN_KEY>
TWITTER_ACCESS_TOKEN_SECRET=<TWITTER_ACCESS_TOKEN_SECRET>
DISCORD_WEBHOOK_ID=<DISCORD_WEBHOOK_ID>
DISCORD_PING_ROLE_ID=<DISCORD_PING_ROLE_ID>
LAST_X_MINUTES_VALUE=30
KEYWORDS="keyword1 keyword2 keyword3"
```

Refer to Discord / Twitter documentation in order to get these values.

### Run script

```shell
> poetry run dotenv run back
```

## Automatic usage

## Requirements

- Setup a Gitlab account and fork this project

## Setup

First, setup the following env variables:

```dotenv
TWITTER_CONSUMER_KEY=<TWITTER_CONSUMER_KEY>
TWITTER_CONSUMER_SECRET=<TWITTER_CONSUMER_SECRET>
TWITTER_ACCESS_TOKEN_KEY=<TWITTER_ACCESS_TOKEN_KEY>
TWITTER_ACCESS_TOKEN_SECRET=<TWITTER_ACCESS_TOKEN_SECRET>
DISCORD_WEBHOOK_ID=<DISCORD_WEBHOOK_ID>
DISCORD_PING_ROLE_ID=<DISCORD_PING_ROLE_ID>
LAST_X_MINUTES_VALUE=30
KEYWORDS="keyword1 keyword2 keyword3"
```

You can do that in the `settings> CI/CD> variables` section of your forked version of this project.

Refer to Discord Twitter documentation in order to get these values.

Then, setup a CI/CD schedule (https://docs.gitlab.com/ee/ci/pipelines/schedules.html) to automatically trigger the
script. You can do so in the `CI/CD> Schedules` section of your forked version of this project. Make sure the frequency
of the cron expression of the schedule matches the `LAST_X_MINUTES_VALUE` env variable if you don't want to miss /
duplicate any tweet.

For example, if you want to run the script every 30 minutes, you will probably also want to check only the tweets of the
last 30 minutes. Thus, you would set `LAST_X_MINUTES_VALUE` to be `30` and the schedule's cron expression to
be `*/30 * * * *`.

### Run script

The script will be automatically triggered according to the CI/CD schedule you configured.

## Example output
![example-output.png](example-output.png)

## Sources used for development

- https://pypi.org/project/discord-webhook/
- https://python-twitter.readthedocs.io/en/latest/