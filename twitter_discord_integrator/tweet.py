from dataclasses import dataclass
from datetime import datetime


@dataclass
class Tweet:
    author: str
    created_at: datetime
    text: str
    id: int
